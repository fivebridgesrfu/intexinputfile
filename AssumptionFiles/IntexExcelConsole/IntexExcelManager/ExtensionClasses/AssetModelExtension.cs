﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntexExcelManager.Models;

namespace IntexExcelManager.ExtensionClasses
{
    public static class AssetModelExtension
    {
        public static bool IsProspectusGroup(this AssetModel assetModel)
        {
            return true;
        }

        public static List<string> Qualifiers(this AssetModel assetModel)
        {
            return assetModel.PricingAssumptions.First().Value.AssumptionSettingByType.Select(setting => setting.Key).ToList();
        }
    }
}
