﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntexExcelManager.Models
{
    [Serializable]
    public class PortfolioAnalysisRunModel
    {
        private const int VersionNumber = 1;
        public int? DocumentVersion { get; set; }

        public int? PortfolioId { get; set; }
        public string PortfolioName { get; set; }

        public DateTime SettlementDate { get; set; }
        public string AnalysisRunId { get; set; }
        public DateTime? AnalysisRunDate { get; set; }
        public List<AssetModel> AssetList { get; set; }
        public List<string> EventLog { get; set; }

        //public PortfolioAnalysisRunModel()
        //{
        //    ModelVersionNumber = VersionNumber;
        //}

    }
}
