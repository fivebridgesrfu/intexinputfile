﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntexExcelManager.Models
{
    public enum AssetType
    {
        [Display(Name = "Unknown")]
        Unknown = 0,

        [Display(Name = "Aircraft Lease")]
        AircraftLease = 1,

        [Display(Name = "Auto Loan and Lease")]
        AutoLoanAndLease = 2,

        [Display(Name = "CDO")]
        Cdo = 3,

        [Display(Name = "CLO")]
        Clo = 4,

        [Display(Name = "CMBS")]
        Cmbs = 5,

        [Display(Name = "Credit Card")]
        CreditCard = 6,

        [Display(Name = "HECM")]
        Hecm = 7,

        [Display(Name = "Manufactured Housing")]
        ManufacturedHousing = 8,

        [Display(Name = "RMBS")]
        Rmbs = 9,

        [Display(Name = "Small Business Loans")]
        SmallBusinessLoans = 10,

        [Display(Name = "Student Loan (FFELP)")]
        StudentLoanFfelp = 11,

        [Display(Name = "Student Loan (Private)")]
        StudentLoanPrivate = 12,

        [Display(Name = "TruPS")]
        TruPs = 13,

        [Display(Name = "Reremic")]
        Reremic = 14,

        [Display(Name = "Other")]
        Other = 98,

        [Display(Name = "All")]
        All = 999

    }
}
