﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntexExcelManager.Models
{
    public enum AssetBucketScheme
    {
        None = 0,
        ProspectusGroupBucketSchemeForTranche = 1,
        SupportProspectusGroupBucketSchemeForTranche = 2,
        GroundGroupBucketSchemeForTranche = 3,
        SupportGroundGroupBucketSchemeForTranche = 4,
        DealLevelCollatGroupBucketScheme = 5,
        GroundGroupBucketSchemeForDeal = 6,
        ProspectusGroupBucketSchemeForDeal = 7,
        ProspectusGroupBucketSchemeForDealWithOther = 8
    }
}
