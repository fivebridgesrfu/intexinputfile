﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntexExcelManager.Models
{
    [Serializable]
    public class AssumptionSettingsModel
    {
        public AssumptionSettingsModel() {
            AssetBucketSchemes = new List<AssetBucketScheme>();
        }


        public string TemplateName { get; set; }
        public DateTime ModifiedDtm { get; set; }
        public int ModifiedBy { get; set; }
        public bool IsInherited { get; set; }
        public string InheritedPortfolioName { get; set; }
        public string InheritedScenarioName { get; set; }
        public DateTime InheritedDtm { get; set; }
        public int InheritedBy { get; set; }

        public Dictionary<string, AssumptionSetting> AssumptionSettingByType { get; set; }
        public bool IsPortfolioAnalysisPopulate { get; set; }

        public List<AssetBucketScheme> AssetBucketSchemes { get; set; } 

    }
}
