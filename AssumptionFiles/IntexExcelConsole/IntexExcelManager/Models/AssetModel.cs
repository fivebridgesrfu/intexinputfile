﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IntexExcelManager.Models
{
    [Serializable]
    public class AssetModel
    {
        public AssetIdentifierTypeEnum AssetIdentifierType { get; set; }

        // VINCE: Rename to AssetIdentifier
        // VINCE: Search code for places where variables are named 'Cusip' and replace with 'AssetIdentifierType' and 'AssetIdentifier'
        public string Cusip { get; set; }

        public AssetType AssetType { get; set; }

        /// <summary>
        /// Asset key that uniquely identifies each parent and child assets 
        /// </summary>
        public string ReferenceId { get; set; }

        public bool? HasStructuredCollateral { get; set; }

        // TODO: Need to remove this after we completely move to json locator schema
        public Dictionary<string, AssumptionSettingsModel> PricingAssumptions = new Dictionary<string, AssumptionSettingsModel>();

//        public readonly Dictionary<string, AnalysisResultsModel> AnalysisResults = new Dictionary<string, AnalysisResultsModel>();

        public List<AssetModel> ChildAssets { get; set; }

        public int? TreeLevel { get; set; }

        public decimal? CurrentContributingBalance { get; set; }

        public long? TreeId { get; set; }

        public long? ParentTreeId { get; set; }

        public decimal? CurrentBalance { get; set; }

        public decimal? OriginalBalance { get; set; }

        public List<string> PendingAssumptions { get; set; }

        public int Id { get; set; }
        public string BloombergName { get; set; }
        public string DealName { get; set; }
        public string TrancheName { get; set; }
        public int? TranchePositionWithinDeal { get; set; }
        public DateTime? SettlementDate { get; set; }
  //      public PriceYieldType? PriceYieldType { get; set; }
        public decimal? PriceYieldValue { get; set; }
    //    public PriceYieldType? YieldMatrixType { get; set; }
        public decimal FaceAmountOwned { get; set; }
        public decimal CurrFaceAmountOwned { get; set; }
        public bool? OTTIFlag { get; set; }
        public decimal? AmortizedCostBasisDollars { get; set; }
        public decimal? AmortizedCostBasisPercent { get; set; }
        public decimal? AccountingYield { get; set; }
        public int Vintage { get; set; }
        public string ParentDealName { get; set; }
        public decimal? ChildContributingPercentageToParent { get; set; }
        public decimal? ChildContributingPercentageToTopLevelParent { get; set; }
        public decimal? ContributingPercentage { get; set; }

        public string DealMarketCategoryName { get; set; }
        public string DealCategory { get; set; }
        public string DealCollatType { get; set; }

        public bool IsSynthetic { get; set; }

        public string DisplayName { 
            get {        
                return new[] { BloombergName, GetIntexName(), Cusip, "Not Found" }.First(x => !string.IsNullOrWhiteSpace(x));  //TODO decide on real display rule
            }
        }
        public List<DealNameTreeLevel> DuplicateChildrensDealNameTreeLevels { get; set; }

        public AssetModel()
        {
            AssetIdentifierType = AssetIdentifierTypeEnum.Cusip;
        }

        public string GetIntexName()
        {
            if(DealName == null || TrancheName == null) return null;
            return string.Concat(DealName, ",", TrancheName);
        }

        public void SetIntexName(string intexName)
        {
            if(string.IsNullOrEmpty(intexName))
            {
                DealName = null;
                TrancheName = null;
                return;
            }

            var regex = new Regex("^([^,]+),([^,]+)$");
            var match = regex.Match(intexName);

            if(!match.Success) throw new FormatException(string.Format("{0} is not in the 'DealName,TrancheName' format as expected", intexName));

            DealName = match.Groups[1].Value;
            TrancheName = match.Groups[2].Value;
        }
        public string PopulateAssumptionGuid { get; set; }
        public bool PricingAssumptionsPopulated { get; set; }
        public bool PricingAssumptionsFailed { get; set; }

    }

    [Serializable]
    public class DealNameTreeLevel
    {
        public int? TreeLevel { get; set; }
        public string ParentDealName { get; set; }
        public decimal? ChildContributingPercentageToTopLevelParent { get; set; }
    }

}