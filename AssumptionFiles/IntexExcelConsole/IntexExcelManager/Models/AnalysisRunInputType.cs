﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntexExcelManager.Models
{
    public enum AnalysisRunInputType
    {
        NewAnalysisRunInput = 1,
        PopulatedAssetScenario = 2
    }
}
