﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntexExcelManager.Models
{
    [Serializable]
    public class PopulatedAssetScenario
    {
        public Dictionary<string, AssumptionSettingsModel> PricingAssumptions { get; set; }
        public Dictionary<string, AssumptionSettingsModel> DealEngineAssumptions { get; set; }
        public Dictionary<string, List<string>> PendingAssumptions { get; set; }

    }
}
