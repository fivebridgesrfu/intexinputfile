﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntexExcelManager.Models
{
    [Serializable]
    public class AssumptionSetting
    {
        public string OverrideValue { get; set; } // if override is not null then use this else get the rules from the populatedSource
        public string PopulatedValue { get; set; }
        public AssumptionSource OverrideSource { get; set; }
        public AssumptionSource PopulatedSource { get; set; }
        public string OverrideChangeReason { get; set; }

    }
}
