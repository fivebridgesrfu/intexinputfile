﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using IntexExcelManager.Models;
using Newtonsoft.Json;

namespace IntexExcelManager
{
    public class TranformAssumptions
    {
        public string AnalysisRunId { get; set; }
        public List<string> Cusips { get; set; }

        public TranformAssumptions(string analysisRunId, List<string> cusips )
        {
            AnalysisRunId = analysisRunId;
            Cusips = cusips;
        }


        public PortfolioAnalysisRunModel GetPortfolioAnalysisRunObject()
        {
            if(String.IsNullOrWhiteSpace(AnalysisRunId))
            {
                return null;
            }

            var portfolioAnalysisRunModel = new PortfolioAnalysisRunModel();
            var assumptionSettingModels = new List<AssumptionSettingsModel>();
            using(JmaDevEntities db = new JmaDevEntities())
            {
                var allInputs = db.AnalysisRunInputs.ToList();
                var analysisRunInputs = allInputs.Where(x => x.AnalysisRunId == AnalysisRunId).ToList();
                if(analysisRunInputs.Any())
                {
                    var newAnalysisRunInput = analysisRunInputs.Single(x => x.InputType == 1);
                    if(newAnalysisRunInput == null || newAnalysisRunInput.DocumentStorage == null || newAnalysisRunInput.DocumentStorage.Document == null)
                        return null;

                    portfolioAnalysisRunModel = JsonConvert.DeserializeObject<PortfolioAnalysisRunModel>(newAnalysisRunInput.DocumentStorage.Document);

                    //get the populated assetScenarios
                    var populatedAnalysisRunIds = analysisRunInputs.Where(x => x.InputType == 2).ToList();
                    var assetsToDelete = portfolioAnalysisRunModel.AssetList.Where(x => !Cusips.Contains(x.Cusip)).ToList();

                    foreach(var assetModel in assetsToDelete)
                    {
                        portfolioAnalysisRunModel.AssetList.Remove(assetModel);
                    }

                    if(populatedAnalysisRunIds.Any())
                        HydrateRunModel(portfolioAnalysisRunModel, populatedAnalysisRunIds);


                }
            }
            return portfolioAnalysisRunModel;
        }


        private void HydrateRunModel(PortfolioAnalysisRunModel portfolioAnalysisRunModel, IEnumerable<AnalysisRunInput> populatedAnalysisRunIds)
        {
            foreach(var container in populatedAnalysisRunIds)
            {
                var parentAsset =
                    portfolioAnalysisRunModel.AssetList.Single(a => a.ReferenceId == container.AssetReferenceId);

                List<AssetModel> assets = null;

                assets = new List<AssetModel>(new[] { parentAsset });
                if(parentAsset.ChildAssets != null) assets.AddRange(parentAsset.ChildAssets);

                var assetDict = new Dictionary<string, AssetModel>();
                assets.ForEach(asset => assetDict.Add(asset.ReferenceId, asset));

                var populatedAssetScenario = JsonConvert.DeserializeObject<PopulatedAssetScenario>(container.DocumentStorage.Document);

                if(populatedAssetScenario.PricingAssumptions != null)
                {
                    foreach(var pair in populatedAssetScenario.PricingAssumptions)
                    {
                        var asset = assetDict[pair.Key];

                        if(asset.PricingAssumptions.ContainsKey(container.ScenarioId))
                        {
                            var newAssumptions = asset.PricingAssumptions[container.ScenarioId];
                            var savedAssumptions = pair.Value;
                            if(!newAssumptions.Equals(savedAssumptions))
                            {
                                asset.PricingAssumptions[container.ScenarioId] = pair.Value;
                            }
                        }
                        else
                            asset.PricingAssumptions.Add(container.ScenarioId, pair.Value);
                    }
                }

                if(populatedAssetScenario.PendingAssumptions != null)
                {
                    foreach(var pair in populatedAssetScenario.PendingAssumptions)
                    {
                        var asset = assets.Single(ca => ca.ReferenceId == pair.Key);
                        if(asset.PendingAssumptions == null)
                            asset.PendingAssumptions = pair.Value;
                        else
                        {
                            if(asset.PendingAssumptions.Count != pair.Value.Count)
                                asset.PendingAssumptions = pair.Value;
                        }
                    }
                }
            }
        }

    }
}
