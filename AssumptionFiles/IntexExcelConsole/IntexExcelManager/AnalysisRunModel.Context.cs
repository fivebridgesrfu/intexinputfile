﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntexExcelManager
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class JmaDevEntities : DbContext
    {
        public JmaDevEntities()
            : base("name=JmaDevEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<AnalysisRunInput> AnalysisRunInputs { get; set; }
        public virtual DbSet<Asset> Assets { get; set; }
        public virtual DbSet<DocumentStorage> DocumentStorages { get; set; }
    }
}
