﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using FBAIntexLib;
using System.IO;
using IntexExcelManager;
using System.Text.RegularExpressions;
using IntexExcelManager.Models;

namespace IntexQRYFiles
{
    class Program
    {
        public bool alreadyLoaded = false;

//        public List<string> prepayUnderlying = new List<string>();
//        public List<string> defaultUnderlying = new List<string>();

        static void Main(string[] args)
        {
            string sDate = "09/30/2015";
            bool isSDateValid = false;

            List<string> prepayProspectus = new List<string>();
            List<string> defaultProspectus = new List<string>();
            List<string> prepayUnderlying = new List<string>();
            List<string> defaultUnderlying = new List<string>();
            List<string> delinqProspectus = new List<string>();
            List<string> delinqUnderlying = new List<string>();

            List<string> groupProspectus = new List<string>();
            List<string> groupUnderlying = new List<string>();

            try
            {
                DateTime dt = DateTime.Parse(sDate);
            }
            catch
            {
                Console.Write("Settle Date: " + sDate + " Is Not Valid!", "Invalid Settle");
                return;
            }

            var analysisRunId = "IN-PROGRESS-cae4fdd9d830b0e9622abcc113d1abfb"; //"IN-PROGRESS-cae4fdd9d830b0e9622abcc113d1abfb"; //e08ddb63-a9c5-4203-863e-085e84c20ddd";
            //"285a7235-5021-4825-a2cb-e6994e43e495"; // "91FFE2F3-3169-4159-A9AE-75E448A9368A"; // "C3716398-F45F-4386-B3F5-6488E584631E";
            var cusips = new List<string>
            {
                "83743YAS2"
                //"86565RAA4"  //"DUKP0LCE0" //"05990AAA6"
            };

            var tranformAssumptions = new TranformAssumptions(analysisRunId, cusips);
            var portfolioAnalysisRun = tranformAssumptions.GetPortfolioAnalysisRunObject();

            using (StreamWriter cdofile = new StreamWriter("c:\\cdo\\83743yas2.qry"))
            {
                cdofile.WriteLine("INTEXCALC_VERSION=3.2");
                cdofile.WriteLine("INTEXCALC_BUILD=21884");
                cdofile.WriteLine("SETTINGS_CREATION_CLIENT=George Colclough");
                cdofile.WriteLine("SETTINGS_CREATION_COMPANY=FIVE BRIDGES ADVISORS, LLC - FORECASTING");
                cdofile.WriteLine("SETTINGS_CREATION_DATE=20160624 01:49:58");
                cdofile.WriteLine(" ");
                cdofile.WriteLine("MODEL_TYPE=SPLIT");
                cdofile.WriteLine("ASSUMP_TYPE=split");
                cdofile.WriteLine("field=CHILDDEAL_PROSPECTUS_GROUP");
                cdofile.WriteLine("START_VALUE=");
                cdofile.WriteLine("ACTUAL_START_VALUE=");
                cdofile.WriteLine("ACTUAL_END_VALUE=");
                cdofile.WriteLine("N_STRINGS=");
                cdofile.WriteLine("BUCKET_SIZE=");
                cdofile.WriteLine("N_BUCKETS=5");
                cdofile.WriteLine("CALC_FIELD=N_BUCKETS");
                cdofile.WriteLine("DISTINCT=false");
                cdofile.WriteLine("COMBOS=[]");
                cdofile.WriteLine("IGNORED={}");
                cdofile.WriteLine("ALIAS={}");
                cdofile.WriteLine("SCALE={}");
                cdofile.WriteLine("CHILD_MODEL[ELSE]={\"START_VALUE\":\"\",\"N_STRINGS\":\"\",\"SCALE\":\"{}\",\"COMBOS\":\"[]\",\"ALIAS\":\"{}\",\"field\":\"UNDER_DEAL\",\"CALC_FIELD\":\"N_BUCKETS\",\"IGNORED\":\"{}\",\"CHILD_MODELS\":\"\",\"ACTUAL_END_VALUE\":\"\",\"BUCKET_SIZE\":\"\",\"DISTINCT\":\"false\",\"ACTUAL_START_VALUE\":\"\",\"N_BUCKETS\":\"5\"}");
                cdofile.WriteLine("CHILD_MODELS=ELSE~");
                cdofile.WriteLine("USE_ASSUMP=1");


                Console.WriteLine(portfolioAnalysisRun.AssetList[0].PricingAssumptions.Values.First().AssumptionSettingByType.Values.First());
                Console.WriteLine(portfolioAnalysisRun.AssetList[0].PricingAssumptions.Count());
                Console.WriteLine(portfolioAnalysisRun.AssetList[0].ChildAssets.Count());

                foreach (var pricingAssumptionPair in portfolioAnalysisRun.AssetList[0].PricingAssumptions.Values)
                {

                    foreach (var assumptionSetting in pricingAssumptionPair.AssumptionSettingByType)
                    {
                        Console.WriteLine(assumptionSetting.Key);
                        Console.WriteLine(assumptionSetting.Value);
                    }
                }
//                Regex regex= new Regex(@"^[A-Z_]*\[(.*)\]$");
                Regex regex = new Regex(@"\[(.*)\]");

                string group_name_list = "";
                string underlyingDeals = "ELSE~";
                int scenario_count = 0;
                portfolioAnalysisRun.AssetList.Sort();
                var childAssets= portfolioAnalysisRun.AssetList[0].ChildAssets.OrderBy(a => a.DealName).ToList();
                /*
                foreach(var childAsset in childAssets)
                {
                    cdofile.WriteLine(childAsset.DealName);
                    Console.WriteLine(childAsset.DealName);
                }
                */
                for (int i = 0; i < portfolioAnalysisRun.AssetList[0].ChildAssets.Count(); i++)
                {

//                    cdofile.WriteLine("");
//                    cdofile.WriteLine(portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName);
                    Console.WriteLine(portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName);

                    var childAsset= portfolioAnalysisRun.AssetList[0].ChildAssets[i];
                    var qualifiers = childAsset.PricingAssumptions.First().Value.AssumptionSettingByType.Select(setting => setting.Key).ToList();

                    Dictionary<string, string> prgGroups = new Dictionary<string,string>();

                    qualifiers.ForEach(q =>
                    {
                        var match = regex.Match(q);
//                        Console.WriteLine("match: {0}", match.Value);
                        if (!prgGroups.ContainsKey(match.Value) && match.Value.Contains("PGRP"))
                        {
                            prgGroups.Add(match.Value, childAsset.DealName);
                        }
                    });

//                    Console.WriteLine("total number of prgGroups = " + prgGroups.Count);
                    /*
                    if (prgGroups.Count() > 1)
                    {
                        foreach(var prgKey in prgGroups.Keys)
                        {
                            if(!prgKey.Contains("0]"))
                            {
                                group_name_list = group_name_list + prgKey.Replace("[AS:PGRP", childAsset.DealName + ":").Replace("]", "") + "~";
                            }
                        }
                    }
                    else
                    {
                        underlyingDeals = underlyingDeals + portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName + "~ELSE|";
                    }
                    */

//                    Console.WriteLine("GroupNames: {0}", qualifiers.Aggregate((s1, s2) => s1 + "," + s2));
                    foreach (var item in prgGroups.Keys)
                    {
                        string prepayRate = "";
                        string prepayRateValue = "";
                        string prepayUnits = "";
                        string prepayUnitsValue = "";

                        string lossRate = "";
                        string lossRateValue = "";
                        string lossUnits = "";
                        string lossUnitsValue = "";
                        string lossSeverity = "";
                        string lossSeverityValue = "";
                        string recoverLag = "";
                        string recoverLagValue = "";
                        string delinqUnits = "";
                        string delinqUnitsValue = "";
                        string delinqRate = "";
                        string delinqRateValue = "";
                        string delinqAdvPrincipal = "";
                        string delinqAdvInterest = "";
                        string delinqRecoveryRate = "";

                        string newkey = "";

                        scenario_count = 0;
                        foreach (var childPricingAssumptionPair in portfolioAnalysisRun.AssetList[0].ChildAssets[i].PricingAssumptions.Values)
                        {
                            foreach (var childAssumptionSetting in childPricingAssumptionPair.AssumptionSettingByType)
                            {
//                                cdofile.WriteLine(childAssumptionSetting.Key);

                                newkey = childAssumptionSetting.Key.Replace("AS:PGRP", portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName + ":");

//                                Console.WriteLine(newkey);


                                if (childAssumptionSetting.Key.Substring(0, 12) == "PREPAY_RATE[" && childAssumptionSetting.Key.Contains(item))
                                {
                                    prepayRate = "\"PREPAY_RATE\"";
                                    prepayRateValue = childAssumptionSetting.Value.PopulatedValue;
                                    /*
                                    int len = childAssumptionSetting.Key.IndexOf("PGRP");
                                    string right = childAssumptionSetting.Key.Substring(len, childAssumptionSetting.Key.Length - len - 1);
                                    string groupName = right.Replace("PGRP", portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName + ":");
                                    if (!(group_name_list.Contains(groupName)))
                                    {
                                        group_name_list = group_name_list + groupName + "\"~\"";
                                    }
                                    prepay_unit_count++;
                                     */
                                }
                                else if(childAssumptionSetting.Key.Substring(0, 13)== "PREPAY_UNITS[" && childAssumptionSetting.Key.Contains(item))
                                {
                                    prepayUnits = "\"PREPAY_UNITS\"";
                                    prepayUnitsValue = childAssumptionSetting.Value.PopulatedValue;
                                }
                                else if (childAssumptionSetting.Key.Substring(0, 11) == "LOSS_UNITS[" && childAssumptionSetting.Key.Contains(item))
                                {
                                    lossUnits = "\"LOSS_UNITS\"";
                                    lossUnitsValue = childAssumptionSetting.Value.PopulatedValue;
                                }
                                else if (childAssumptionSetting.Key.Substring(0, 10) == "LOSS_RATE[" && childAssumptionSetting.Key.Contains(item))
                                {
                                    lossRate = "\"LOSS_RATE\"";
                                    lossRateValue = childAssumptionSetting.Value.PopulatedValue;
                                }
                                else if(childAssumptionSetting.Key.Substring(0, 14) == "LOSS_SEVERITY[" && childAssumptionSetting.Key.Contains(item))
                                {
                                    lossSeverity = "\"LOSS_SEVERITY\"";
                                    lossSeverityValue = childAssumptionSetting.Value.PopulatedValue;
                                }
                                else if (childAssumptionSetting.Key.Substring(0, 12) == "RECOVER_LAG[" && childAssumptionSetting.Key.Contains(item))
                                {
                                    recoverLag = "\"RECOVER_LAG\"";
                                    recoverLagValue = childAssumptionSetting.Value.PopulatedValue;
                                }
                                else if (childAssumptionSetting.Key.Substring(0, 13) == "DELINQ_UNITS[" && childAssumptionSetting.Key.Contains(item))
                                {
                                    delinqUnits = "\"DELINQ_UNITS\"";
                                    delinqUnitsValue = childAssumptionSetting.Value.PopulatedValue;
                                }
                                else if (childAssumptionSetting.Key.Substring(0, 12) == "DELINQ_RATE[" && childAssumptionSetting.Key.Contains(item))
                                {
                                    delinqRate = "\"DELINQ_RATE\"";
                                    delinqRateValue = childAssumptionSetting.Value.PopulatedValue;
                                    delinqUnits = "\"DELINQ_UNITS\"";
                                    delinqUnitsValue = "PCT";
                                    delinqAdvPrincipal = "0";
                                    delinqAdvInterest = "0";
                                }
//                                else if (childAssumptionSetting.Key.Substring(0, 20) == "DELINQ_RECOVER_RATE[" && childAssumptionSetting.Key.Contains(item))
                                else if (childAssumptionSetting.Key.Substring(0, 14) == "DELINQ_RECOVER" && childAssumptionSetting.Key.Contains(item))
                                {
                                    delinqRecoveryRate = childAssumptionSetting.Value.PopulatedValue;
                                }
                            }

                            if (scenario_count < 1)
                            {
                                if (prgGroups.Count() < 2)
                                {
                                    if (prepayRate == "")
                                    {
                                        prepayRate = "PREPAY_RATE";
                                        prepayRateValue = "0";
                                    }
                                    if (lossSeverity == "")
                                    {
                                        lossSeverity = "LOSS_SEVERITY";
                                        lossSeverityValue = "100";
                                    }
                                        //cdofile.WriteLine("ASSUMP_PREPAY" + item.Replace("AS:PGRP", portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName + ":") + "={" + prepayUnits + ":\"" + prepayUnitsValue + "\"" + prepayRate + ":\"" + prepayRateValue + "\"}");
                                        //cdofile.WriteLine("ASSUMP_DEFAULT" + item.Replace("AS:PGRP", portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName + ":") + "={" + lossUnits + ":\"" + lossUnitsValue + "\"" + lossRate + ":\"" + lossRateValue + "\",\"#SEVERITY_UNITS\":\"Percent\"," + lossSeverity + ":\"" + lossSeverityValue + "\",\"#RECOVER_LAG_UNITS\":\"Months\"," + recoverLag + ":\"" + recoverLagValue + "\"}");
                                    string localPrepay = "ASSUMP_PREPAY" + item.Replace("AS:PGRP:0", "ELSE|" + portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName).Replace("\"", "") + "={" + prepayUnits + ":\"" + prepayUnitsValue + "\"," + prepayRate + ":\"" + prepayRateValue + "\"}";
                                    string localDefault = "ASSUMP_DEFAULT" + item.Replace("AS:PGRP:0", "ELSE|" + portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName).Replace("\"", "") + "={" + lossUnits + ":\"" + lossUnitsValue + "\"," + lossRate + ":\"" + lossRateValue + "\",\"#SEVERITY_UNITS\":\"Percent\"," + lossSeverity + ":\"" + lossSeverityValue + "\",\"#RECOVER_LAG_UNITS\":\"Months\"," + recoverLag + ":\"" + recoverLagValue + "\"}";
                                    string localDelinq = "ASSUMP_DELINQUENCY" + item.Replace("AS:PGRP:0", "ELSE|" + portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName).Replace("\"", "") + "={" + delinqUnits + ":\"" + delinqUnitsValue + "\"," + delinqRate + ":\"" + delinqRateValue + "\",\"#DELINQ_ADV_PCT_P\":\"" + delinqAdvPrincipal + "\",\"#DELINQ_ADV_PCT_I\":\"" + delinqAdvInterest + "\",\"DELINQ_RECOVER_RATE\":\"" + delinqRecoveryRate + "\"}";

                                    prepayUnderlying.Add(localPrepay);
                                    defaultUnderlying.Add(localDefault);
                                    delinqUnderlying.Add(localDelinq);
                                    groupUnderlying.Add(item.Replace("[AS:PGRP:0", "ELSE|" + portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName));
                                }
                                else
                                {
                                    if (!item.Contains("PGRP:0]"))
                                    {
                                        if (prepayRate == "")
                                        {
                                            prepayRate = "PREPAY_RATE";
                                            prepayRateValue = "0";
                                        }
                                        if (lossSeverity == "")
                                        {
                                            lossSeverity = "LOSS_SEVERITY";
                                            lossSeverityValue = "100";
                                        }
                                        //cdofile.WriteLine("ASSUMP_PREPAY" + item.Replace("AS:PGRP", portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName + ":") + "={" + prepayUnits + ":\"" + prepayUnitsValue + "\"" + prepayRate + ":\"" + prepayRateValue + "\"}");
                                        //cdofile.WriteLine("ASSUMP_DEFAULT" + item.Replace("AS:PGRP", portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName + ":") + "={" + lossUnits + ":\"" + lossUnitsValue + "\"" + lossRate + ":\"" + lossRateValue + "\",\"#SEVERITY_UNITS\":\"Percent\"," + lossSeverity + ":\"" + lossSeverityValue + "\",\"#RECOVER_LAG_UNITS\":\"Months\"," + recoverLag + ":\"" + recoverLagValue + "\"}");
                                        string localPrepay = "ASSUMP_PREPAY" + item.Replace("AS:PGRP", portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName + ":").Replace("\"", "") + "={" + prepayUnits + ":\"" + prepayUnitsValue + "\"," + prepayRate + ":\"" + prepayRateValue + "\"}";
                                        string localDefault = "ASSUMP_DEFAULT" + item.Replace("AS:PGRP", portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName + ":").Replace("\"", "") + "={" + lossUnits + ":\"" + lossUnitsValue + "\"," + lossRate + ":\"" + lossRateValue + "\",\"#SEVERITY_UNITS\":\"Percent\"," + lossSeverity + ":\"" + lossSeverityValue + "\",\"#RECOVER_LAG_UNITS\":\"Months\"," + recoverLag + ":\"" + recoverLagValue + "\"}";
                                        string localDelinq = "ASSUMP_DELINQUENCY" + item.Replace("AS:PGRP", portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName + ":").Replace("\"", "") + "={" + delinqUnits + ":\"" + delinqUnitsValue + "\"," + delinqRate + ":\"" + delinqRateValue + "\",\"#DELINQ_ADV_PCT_P\":\"" + delinqAdvPrincipal + "\",\"#DELINQ_ADV_PCT_I\":\"" + delinqAdvInterest + "\",\"DELINQ_RECOVER_RATE\":\"" + delinqRecoveryRate + "\"}";
                                        prepayProspectus.Add(localPrepay);
                                        defaultProspectus.Add(localDefault);
                                        delinqProspectus.Add(localDelinq);
                                        groupProspectus.Add(item.Replace("[AS:PGRP", portfolioAnalysisRun.AssetList[0].ChildAssets[i].DealName + ":"));
                                    }
                                }
                                scenario_count = 2;
                            }
                        }

                    }
                    //                }
                }

                cdofile.WriteLine("ASSUMP_PREPAY={\"PREPAY_UNITS\":\"CPR\",\"PREPAY_RATE\":\"0\"}");
                cdofile.WriteLine("ASSUMP_DEFAULT={\"LOSS_UNITS\":\"CDR\",\"LOSS_RATE\":\"0\",\"#SEVERITY_UNITS\":\"Percent\",\"LOSS_SEVERITY\":\"100\",\"#RECOVER_LAG_UNITS\":\"Months\",\"RECOVER_LAG\":\"0\"}");
                cdofile.WriteLine("ASSUMP_DELINQUENCY={\"DELINQ_UNITS\":\"PCT\",\"DELINQ_RATE\":\"0\",\"#DELINQ_ADV_PCT_P\":\"0\",\"#DELINQ_ADV_PCT_I\":\"0\",\"DELINQ_RECOVER_RATE\":\"0\"}");

                defaultProspectus.Sort();
                prepayProspectus.Sort();
                delinqProspectus.Sort();
                groupProspectus.Sort();

                for (int i = 0; i < prepayProspectus.Count(); i++)
                {
                    cdofile.WriteLine(prepayProspectus[i]);
                    cdofile.WriteLine(defaultProspectus[i]);
                    cdofile.WriteLine(delinqProspectus[i]);
                    group_name_list = group_name_list + groupProspectus[i].Replace("]", "~").Replace("\"", "");
                }

                cdofile.WriteLine("ASSUMP_PREPAY[ELSE]={\"PREPAY_UNITS\":\"CPR\",\"PREPAY_RATE\":\"0\"}");
                cdofile.WriteLine("ASSUMP_DEFAULT[ELSE]={\"LOSS_UNITS\":\"CDR\",\"LOSS_RATE\":\"0\",\"#SEVERITY_UNITS\":\"Percent\",\"LOSS_SEVERITY\":\"100\",\"#RECOVER_LAG_UNITS\":\"Months\",\"RECOVER_LAG\":\"0\"}");
                cdofile.WriteLine("ASSUMP_DELINQUENCY[ELSE]={\"DELINQ_UNITS\":\"PCT\",\"DELINQ_RATE\":\"0\",\"#DELINQ_ADV_PCT_P\":\"0\",\"#DELINQ_ADV_PCT_I\":\"0\",\"DELINQ_RECOVER_RATE\":\"0\"}");

                prepayUnderlying.Sort();
                defaultUnderlying.Sort();
                delinqUnderlying.Sort();
                groupUnderlying.Sort();

                for (int i = 0; i < prepayUnderlying.Count(); i++)
                {
                    cdofile.WriteLine(prepayUnderlying[i]);
                    cdofile.WriteLine(defaultUnderlying[i]);
                    cdofile.WriteLine(delinqUnderlying[i]);
                    underlyingDeals = underlyingDeals + groupUnderlying[i].Replace("]", "~").Replace("\"", "");
                }

                cdofile.WriteLine("ASSUMP_PREPAY[ELSE|ELSE]={\"PREPAY_UNITS\":\"CPR\",\"PREPAY_RATE\":\"0\"}");
                cdofile.WriteLine("ASSUMP_DEFAULT[ELSE|ELSE]={\"LOSS_UNITS\":\"CDR\",\"LOSS_RATE\":\"0\",\"#SEVERITY_UNITS\":\"Percent\",\"LOSS_SEVERITY\":\"100\",\"#RECOVER_LAG_UNITS\":\"Months\",\"RECOVER_LAG\":\"0\"}");
                cdofile.WriteLine("ASSUMP_DELINQUENCY[ELSE|ELSE]={\"DELINQ_UNITS\":\"PCT\",\"DELINQ_RATE\":\"0\",\"#DELINQ_ADV_PCT_P\":\"0\",\"#DELINQ_ADV_PCT_I\":\"0\",\"DELINQ_RECOVER_RATE\":\"0\"}");

                cdofile.WriteLine("BUCKET_NAMES=" + group_name_list + underlyingDeals + "ELSE|ELSE~");
                cdofile.WriteLine("TREAT_SEVERITY_AS_RECOVERY=0");
                cdofile.WriteLine("QUERY_FORECAST_LAYOUT={Remove Scenarios={use=false}, Reset Scenarios={use=false}, Toggle Scenarios={use=true}, Rates={scenario=false}, Rate Shocks={scenario=true, use=false}, Third Party Model={use=false}, Prepay={scenario=true, use=true}, Prepay Penalty Haircut={detail=true, scenario=true, use=false, group=false}, Prepay Monthly on non-Monthly Collat={detail=true, scenario=true, use=false, group=true}, Prepay Convention={detail=true, scenario=false, use=false, group=false}, Prepay Already Defeased Assets={detail=true, scenario=false, use=true, group=false}, Disregard prepay restrictions and penalties={detail=true, scenario=true, use=false, group=true}, Prepay Style={detail=true, scenario=true, use=false, group=true}, Default={scenario=true, use=true}, Severity={detail=false, scenario=true, use=true, group=true}, Unadvanced Interest Recovery (%)={detail=true, scenario=true, use=false, group=true}, Default Adv Pct={detail=true, scenario=true, use=false, group=true}, Recovery Lag={detail=false, scenario=true, use=true, group=true}, Recovery Timeseries={detail=true, scenario=true, use=false, group=true}, Liquidation Timeseries={detail=true, scenario=true, use=false, group=true}, Advance Detail={detail=true, scenario=false, use=false, group=false}, Default Monthly on non-Monthly Collat={detail=true, scenario=true, use=false, group=true}, Default at Balloon={detail=true, scenario=true, use=false, group=true}, Monthly Rate during Init Recov Lag={detail=true, scenario=true, use=false, group=true}, Severity during Init Recov Lag={detail=true, scenario=true, use=false, group=true}, Severity or Recovery is Gross={detail=true, scenario=true, use=false, group=true}, Special ASER&Non-Recov Handling={scenario=true, use=false}, Collect Special Servicing Fees={scenario=true, use=false}, Loan Modification={}, Rate Modification={scenario=true, use=false}, Balance Forgiveness Rate={scenario=true, use=false}, Balance Capitalization Rate={scenario=true, use=false}, Forbearance Rate={scenario=true, use=false}, Amort Profile={scenario=true, use=false}, ARM Margin={use=false}, Do not fix modified ARM loans={scenario=true, use=false}, Floor floating-rate assets at their current rate={scenario=true, use=true}, Mod Servicing Fee={scenario=true, use=false}, WAC Deterioration={scenario=true, use=false}, Interest Adjustment={scenario=true, use=false}, Interest Rate at Reset (fixed reset loans)={scenario=true, use=false}, Delinquency={scenario=true, use=true}, Delinq Adv Pct={detail=true, scenario=true, use=true, group=true}, Delinq Recovery Rate={detail=true, scenario=true, use=true, group=true}, Draw={scenario=true, use=false}, Balloon Extension={scenario=true, use=false}, Extend Pct of Principal={detail=true, scenario=true, use=false}, Coupon Increase for Extension (%)={detail=true, scenario=true, use=false}, Extension Amort Type={detail=true, scenario=true, use=false}, Optional Redemption={scenario=true, use=true}, Hedge Terminations={scenario=true, use=false}, Trigger Overrides={scenario=true, use=false}, Credit Enhancement={scenario=true, use=false}, Student Loan Overrides={scenario=true, use=false}, Student Loan Target Status (%)={scenario=true, use=true}, Reinvestment Profile={scenario=true, use=true}, Coupon-Spread={detail=false, scenario=true}, Maturity-Mat. Date={detail=false, scenario=true, use=true}, Price={detail=true, scenario=true, use=true}, Life Floor={detail=true, scenario=true, use=true}, Asset Type={detail=true, scenario=true, use=false}, Asset Subtype={}, Minimum Rating={detail=false, scenario=false, use=false}, Moody's Industry Name={detail=false, scenario=false, use=false}, Amortization={detail=true, scenario=true, use=false}, Daycount={detail=true, scenario=true, use=false}, Frequency={detail=true, scenario=true, use=false}, PIK Rate={detail=true, scenario=true, use=false}, Intra-Period Reinvestment={use=true}, During Reinvest %={detail=true, scenario=true}, After Reinvest %={detail=true, scenario=true}, Model Variables={scenario=true, use=true}, Child Deal Model Variables={scenario=true, use=true}, Asset Rating Overrides={}, Mod Moody's Rating={scenario=true, use=false}, Mod S&P Rating={scenario=true, use=false}, Mod Fitch Rating={scenario=true, use=false}, Subsequent Recoveries={scenario=true, use=false}, Collection Account Holdback={scenario=true, use=false}, Starting Collection Acct Balance (CDOs)={scenario=true, use=false}, Selling of Collateral (CDOs and revenue assets)={use=false}, Solver={scenario=true, use=true}, Scenario Multipliers={use=false}}");
                cdofile.WriteLine("QUERY_FORECAST_LAYOUT_STR={Default={bucketscheme=}, Delinquency={delinq_type=Affects CFs & Triggers - Entire Balance}}");


            }


            for(int i = 0; i < prepayUnderlying.Count(); i++)
            {
                Console.WriteLine(prepayUnderlying[i]);
                Console.WriteLine(defaultUnderlying[i]);
            }

        }
    }

    public static class AssetModelExtension
    {
        public static bool IsProspectusGroup(this AssetModel assetModel)
        {
            return true;
        }

        public static List<string> Qualifiers(this AssetModel assetModel)
        {
            return assetModel.PricingAssumptions.First().Value.AssumptionSettingByType.Select(setting => setting.Key).ToList();
        }
    }
}
