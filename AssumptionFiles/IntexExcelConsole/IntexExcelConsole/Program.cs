﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntexExcelManager;

namespace IntexExcelConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            var analysisRunId = "C3716398-F45F-4386-B3F5-6488E584631E";
            var cusips = new List<string>
            {
                "05990AAA6"
            };

            var tranformAssumptions = new TranformAssumptions(analysisRunId, cusips);
            var portfolioAnalysisRun = tranformAssumptions.GetPortfolioAnalysisRunObject();
            
            Console.ReadLine();
        }
    }
}
